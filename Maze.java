public class Maze {
	private final int TRIED = 3;
	private final int PATH = 7;
	private int[][] grid =
		{{1,1,1,0,1,1,0,0,0,1,1,1,1},
		 {1,0,1,1,1,0,1,1,1,1,0,0,1},
		 {0,0,0,0,1,0,1,0,1,0,1,0,0},
		 {1,1,1,0,1,1,1,0,1,0,1,1,1},
		 {1,0,1,0,0,0,0,1,1,1,0,0,1},
		 {1,0,1,1,1,1,1,1,0,1,1,1,1},
		 {1,0,0,0,0,0,0,0,0,0,0,0,0},
		 {1,1,1,1,1,1,1,1,1,1,1,1,1}
		};
	
	public boolean traverse (int row, int column) {
		boolean done = false;
		if (valid (row, column)) {

	         grid[row][column] = 3;  // cell has been tried

	         if (row == grid.length-1 && column == grid[0].length-1)
	            done = true;  // maze is solved
	         else {
	            done = traverse (row+1, column);  // down
	            if (!done)
	               done = traverse (row, column+1);  // right
	            if (!done)
	               done = traverse (row-1, column);  // up
	            if (!done)
	               done = traverse (row, column-1);  // left
	         }
	         if (done)  // part of the final path
	            grid[row][column] = 7;
	      }
	      
	      return done;
		}
		//tentukan jika lokasi spesifik adalah valid
		private boolean valid1 (int row, int column) {
			boolean result = false;
			 
		      // check if cell is in the bounds of the matrix
		      if (row >= 0 && row < grid.length &&
		          column >= 0 && column < grid[0].length)

		         //  check if cell is not blocked and not previously tried
		         if (grid[row][column] == 1)
		            result = true;

		      return result;
			}
		//kembalikan nilai maze sebagai string
		public String toString() {
				String result = "\n";
			  System.out.println();

		      for (int row=0; row < grid.length; row++) {
		         for (int column=0; column < grid[row].length; column++)
		            System.out.print (grid[row][column]);
		         System.out.println();
		      }

		      System.out.println();
		      return result;
	}

	private boolean valid(int row, int column) {
		// TODO Auto-generated method stub
		return false;
	}
}
